-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2020 at 07:16 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `username`, `password`, `create_date`, `create_at`, `update_at`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-17', '2020-02-17 10:50:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_card`
--

CREATE TABLE `tbl_card` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `create_date` date DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_card`
--

INSERT INTO `tbl_card` (`id`, `title`, `create_date`, `create_at`, `update_at`, `file_name`) VALUES
(1, 'Garena Shell', '2020-02-18', '2020-02-18 20:35:09', NULL, 'card_game-1582032909.png'),
(2, 'Razer pin', '2020-02-18', '2020-02-18 20:46:37', '2020-02-19 01:07:25', 'card_game-1582033597.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_card_game`
--

CREATE TABLE `tbl_card_game` (
  `id` int(11) NOT NULL,
  `id_card` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_card_game`
--

INSERT INTO `tbl_card_game` (`id`, `id_card`, `id_game`, `create_date`, `create_at`, `update_at`) VALUES
(1, 1, 1, '2020-02-18', '2020-02-18 20:35:09', NULL),
(2, 1, 2, '2020-02-18', '2020-02-18 20:35:09', NULL),
(3, 1, 3, '2020-02-18', '2020-02-18 20:35:09', NULL),
(4, 1, 4, '2020-02-18', '2020-02-18 20:35:09', NULL),
(5, 1, 5, '2020-02-18', '2020-02-18 20:35:09', NULL),
(8, 2, 3, '2020-02-19', '2020-02-19 01:07:25', NULL),
(9, 2, 5, '2020-02-19', '2020-02-19 01:07:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_card_price`
--

CREATE TABLE `tbl_card_price` (
  `id` int(11) NOT NULL,
  `id_card` int(11) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_card_price`
--

INSERT INTO `tbl_card_price` (`id`, `id_card`, `price`, `point`, `create_date`, `create_at`, `update_at`) VALUES
(1, 1, '20', '27', '2020-02-18', '2020-02-18 20:35:09', NULL),
(2, 1, '50', '67', '2020-02-18', '2020-02-18 20:35:09', NULL),
(3, 1, '90', '121', '2020-02-18', '2020-02-18 20:35:09', NULL),
(4, 1, '100', '135', '2020-02-18', '2020-02-18 20:35:09', NULL),
(5, 1, '300', '405', '2020-02-18', '2020-02-18 20:35:09', NULL),
(6, 1, '500', '675', '2020-02-18', '2020-02-18 20:35:09', NULL),
(7, 1, '1000', '1350', '2020-02-18', '2020-02-18 20:35:09', NULL),
(12, 2, '50', '105', '0000-00-00', '2020-02-19 01:07:25', NULL),
(13, 2, '300', '660', '0000-00-00', '2020-02-19 01:07:25', NULL),
(14, 2, '500', '1150', '0000-00-00', '2020-02-19 01:07:25', NULL),
(15, 2, '1000', '2320', '0000-00-00', '2020-02-19 01:07:25', NULL),
(16, 2, '1500', '3550', '0000-00-00', '2020-02-19 01:07:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_game`
--

CREATE TABLE `tbl_game` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_cover` varchar(255) DEFAULT NULL COMMENT '// รูปรายละเอียดการเติมเกมส์ของแต่ละอัน',
  `url` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_game`
--

INSERT INTO `tbl_game` (`id`, `title`, `file_name`, `file_cover`, `url`, `create_date`, `create_at`, `update_at`) VALUES
(1, 'Heroes of Newerth', 'game-1582032548.jpg', 'game-1582032548.png', '', '2020-02-18', '2020-02-18 20:29:08', NULL),
(2, 'Fifa Online 4', 'game-1582032571.png', 'game-15820325711.png', '', '2020-02-18', '2020-02-18 20:29:31', NULL),
(3, 'Free Fire', 'game-1582171353.jpg', 'game-15820325881.jpg', '', '2020-02-18', '2020-02-18 20:29:48', '2020-02-20 11:02:33'),
(4, 'Blade and soul', 'game-1582032642.jpg', 'game-15820326421.jpg', '', '2020-02-18', '2020-02-18 20:30:42', NULL),
(5, 'PUBG LITE', 'game-1582032666.png', 'game-1582032666.jpg', '', '2020-02-18', '2020-02-18 20:31:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL,
  `order_number` varchar(255) NOT NULL,
  `id_card` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_promotion`
--

CREATE TABLE `tbl_promotion` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `file_cover` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_promotion`
--

INSERT INTO `tbl_promotion` (`id`, `title`, `detail`, `file_cover`, `create_date`, `create_at`, `update_at`) VALUES
(1, 'เราคือตัวแทนจำหน่าย บัตรเติมเงินเกมต่างๆ มากมาย', '', 'promotion-1582187654.png', '2020-02-18', '2020-02-18 20:28:14', '2020-02-20 15:35:18'),
(2, 'รับเติมเกมมือถือออนไลน์ทุกชนิด', '', 'promotion-1582187702.jpg', '2020-02-18', '2020-02-18 20:28:31', '2020-02-20 15:35:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_card`
--
ALTER TABLE `tbl_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_card_game`
--
ALTER TABLE `tbl_card_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_card_price`
--
ALTER TABLE `tbl_card_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_game`
--
ALTER TABLE `tbl_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_promotion`
--
ALTER TABLE `tbl_promotion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_card`
--
ALTER TABLE `tbl_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_card_game`
--
ALTER TABLE `tbl_card_game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_card_price`
--
ALTER TABLE `tbl_card_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_game`
--
ALTER TABLE `tbl_game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_promotion`
--
ALTER TABLE `tbl_promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
