/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : game

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-02-24 17:48:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_admin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_admin
-- ----------------------------
INSERT INTO `tbl_admin` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2020-02-17', '2020-02-17 10:50:45', '2020-02-21 11:59:56');

-- ----------------------------
-- Table structure for `tbl_card`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_card`;
CREATE TABLE `tbl_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `create_date` date DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_card
-- ----------------------------
INSERT INTO `tbl_card` VALUES ('3', 'Google Play', '2020-02-24', '2020-02-24 13:33:11', null, 'card_game-1582525991.jpg');

-- ----------------------------
-- Table structure for `tbl_card_game`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_card_game`;
CREATE TABLE `tbl_card_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_card` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_card_game
-- ----------------------------
INSERT INTO `tbl_card_game` VALUES ('1', '3', '1', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('2', '3', '2', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('3', '3', '3', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('4', '3', '4', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('5', '3', '5', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('6', '3', '6', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('7', '3', '7', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_game` VALUES ('8', '3', '8', '2020-02-24', '2020-02-24 13:33:11', null);

-- ----------------------------
-- Table structure for `tbl_card_price`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_card_price`;
CREATE TABLE `tbl_card_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_card` int(11) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `point` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_card_price
-- ----------------------------
INSERT INTO `tbl_card_price` VALUES ('1', '3', '300', '300', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_price` VALUES ('2', '3', '500', '500', '2020-02-24', '2020-02-24 13:33:11', null);
INSERT INTO `tbl_card_price` VALUES ('3', '3', '2000', '2000', '2020-02-24', '2020-02-24 13:33:11', null);

-- ----------------------------
-- Table structure for `tbl_game`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_game`;
CREATE TABLE `tbl_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_cover` varchar(255) DEFAULT NULL COMMENT '// รูปรายละเอียดการเติมเกมส์ของแต่ละอัน',
  `url` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_game
-- ----------------------------
INSERT INTO `tbl_game` VALUES ('1', 'marvel super war', 'game-1582520031.png', 'game-1582520031.jpg', 'facebook.com/24buym/photos/a.482107481810799/3180107188677468/?type=3&theater', '2020-02-24', '2020-02-24 11:53:51', '2020-02-24 12:01:47');
INSERT INTO `tbl_game` VALUES ('2', 'Ragnarok M', 'game-1582520286.jpg', 'game-1582524843.jpg', 'facebook.com/24buym/photos/a.482107481810799/3167645996590254/?type=3&theater', '2020-02-24', '2020-02-24 11:58:06', '2020-02-24 13:14:03');
INSERT INTO `tbl_game` VALUES ('3', 'Free Fire', 'game-1582520789.png', 'game-15825207131.jpg', 'facebook.com/24buym/photos/a.482107481810799/3164748683546652/?type=3&theater', '2020-02-24', '2020-02-24 12:05:13', '2020-02-24 12:06:29');
INSERT INTO `tbl_game` VALUES ('4', 'Arena of valor', 'game-1582521045.jpg', 'game-1582521382.jpg', 'facebook.com/24buym/photos/a.482107481810799/3145307545490766/?type=3&theater', '2020-02-24', '2020-02-24 12:10:45', '2020-02-24 12:16:22');
INSERT INTO `tbl_game` VALUES ('5', 'PUB G Mobile', 'game-1582521520.jpg', 'game-15825215201.jpg', 'facebook.com/24buym/photos/a.482107481810799/3137760589578795/?type=3&theater', '2020-02-24', '2020-02-24 12:18:40', null);
INSERT INTO `tbl_game` VALUES ('6', 'Garena Speed Drifters TH', 'game-1582522014.jpg', 'game-15825219611.jpg', 'facebook.com/24buym/photos/a.482107481810799/2852411141447076/?type=3&theater', '2020-02-24', '2020-02-24 12:26:01', '2020-02-24 12:26:54');
INSERT INTO `tbl_game` VALUES ('7', 'Ulala: Idle Adventure ', 'game-1582522114.jpg', 'game-15825221141.jpg', 'facebook.com/24buym/photos/a.482107481810799/2808703065817884/?type=3&theater', '2020-02-24', '2020-02-24 12:28:34', null);
INSERT INTO `tbl_game` VALUES ('8', 'LaPlace M ', 'game-1582522251.jpg', 'game-15825222511.jpg', 'facebook.com/24buym/photos/a.482107481810799/2671158212905704/?type=3&theater', '2020-02-24', '2020-02-24 12:30:51', null);

-- ----------------------------
-- Table structure for `tbl_order`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(255) NOT NULL,
  `id_card` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  `update_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_order
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_promotion`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_promotion`;
CREATE TABLE `tbl_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `file_cover` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_promotion
-- ----------------------------
INSERT INTO `tbl_promotion` VALUES ('2', 'เปิดให้บริการตามปกติแล้ว', null, 'https://www.facebook.com/24buym/photos/a.482107481810799/3058190280869160/?type=3&theater', 'promotion-1582521727.jpg', '2020-02-24', '2020-02-24 12:22:07', '2020-02-24 15:40:33');
INSERT INTO `tbl_promotion` VALUES ('3', 'ถ้าได้UCจะเอาไปทำอะไร?? **เจอกันพรุ่งนี้เวลา 06:00-12:00 นะคะ **', null, 'https://www.facebook.com/jackfruitchannel/photos/a.423761911758155/616310859169925/?type=3&theater', 'promotion-1582521743.jpg', '2020-02-24', '2020-02-24 12:22:23', '2020-02-24 15:43:37');

-- ----------------------------
-- Table structure for `tbl_slider`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_slider`;
CREATE TABLE `tbl_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL,
  `create_date` date NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_slider
-- ----------------------------
