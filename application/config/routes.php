<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'front-end/Home_ctr';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['index']             = 'front-end/Home_ctr';
$route['card_game']         = 'front-end/Card_game_ctr';
$route['card_game_detail']  = 'front-end/Card_game_ctr/card_game_detail';
$route['selectCard_game']   = 'front-end/Card_game_ctr/select_card_game';
$route['selectCount_game']  = 'front-end/Card_game_ctr/select_count_game';
$route['order_card']        = 'front-end/Card_game_ctr/order_card';
$route['top_up_game']       = 'front-end/Topup_game_ctr';
$route['pomotion']          = 'front-end/Pomotion_ctr';
$route['test']              = 'front-end/Home_ctr/test';
// BACKEND //
$route['backend-login']                 = 'back-end/Login_ctr';
$route['backend-loginMe']               = 'back-end/Login_ctr/loginMe';
$route['backend-dashboard']             = 'back-end/Dashboard_ctr';
$route['backend-promotion']             = 'back-end/Promotion_ctr';
$route['backend-promotion-add']         = 'back-end/Promotion_ctr/promotion_add';
$route['backend-promotion-delete']      = 'back-end/Promotion_ctr/delete_promotion';
$route['backend-promotion-update']      = 'back-end/Promotion_ctr/update_promotion';
$route['backend-uploads-file']          = 'back-end/Promotion_ctr/upload_image';
$route['backend-cardgame']              = 'back-end/Dashboard_ctr/card_game';
$route['backend-card-game-add']         = 'back-end/Dashboard_ctr/card_game_add';
$route['backend-card-game-edit']        = 'back-end/Dashboard_ctr/card_game_edit';
$route['backend-card-game-delete']      = 'back-end/Dashboard_ctr/card_game_delete';
$route['backend-change-game']           = 'back-end/Dashboard_ctr/change_game';
$route['backend-game']                  = 'back-end/Game_ctr';
$route['backend-game-add']              = 'back-end/Game_ctr/game_add';
$route['backend-game-update']           = 'back-end/Game_ctr/game_update';
$route['backend-game-delete']           = 'back-end/Game_ctr/game_delete';
$route['backend-logout']                = 'back-end/Login_ctr/logout';
$route['backend-repass']                = 'back-end/Login_ctr/re_password';
$route['backend-order']                 = 'back-end/Order_ctr';
$route['backend-slider']                = 'back-end/Slider_ctr';
$route['backend-slider-add']            = 'back-end/Slider_ctr/slider_add';
$route['backend-slider-update']         = 'back-end/Slider_ctr/slider_update';
$route['backend-slider-delete']         = 'back-end/Slider_ctr/slider_delete';