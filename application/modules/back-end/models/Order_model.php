<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/* Author: Jorge Torres
 * Description: Login model class
 */
class Order_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function get_order()
    {
        $this->db->select('*');
        $this->db->from('tbl_order');
        $this->db->order_by('create_at', 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }
}
