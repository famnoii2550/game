<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Game_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        $data['game_list']  = $this->db->order_by('create_at','DESC')->get('tbl_game')->result_array();
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $this->load->view('options/header');
            $this->load->view('games-list', $data);
            $this->load->view('options/footer');
        }
    }

    function game_add()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $title              = $this->input->post('title');
            $url                = $this->input->post('url');
            $create_date        = date('Y-m-d');
            $create_at          = date('Y-m-d H:i:s');

            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/game';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "game-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_name']['name']) {
                if ($this->upload->do_upload('file_name')) {

                    $gamber     = $this->upload->data();
                    $data = array(
                        'title'             => $title,
                        'file_name'         => $gamber['file_name'],
                        'url'               => $url,
                        'create_date'       => $create_date,
                        'create_at'         => $create_at,
                    );
                    if ($this->db->insert('tbl_game', $data)) {
                        $insert_id = $this->db->insert_id();
                        // |xlsx|pdf|docx
                        $config2['upload_path'] = 'uploads/game';
                        $config2['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config2['max_size']     = '200480';
                        $config2['max_width'] = '5000';
                        $config2['max_height'] = '5000';
                        $config2['width'] = 800;
                        $config2['height'] = 533;
                        $name_file2 = "game-" . time();
                        $config2['file_name'] = $name_file2;

                        $this->upload->initialize($config2);

                        $data2 = array();

                        if ($_FILES['file_cover']['name']) {
                            if ($this->upload->do_upload('file_cover')) {
                                $gamber2     = $this->upload->data();
                                $dataU = array(
                                    'file_cover'        => $gamber2['file_name'],
                                );
                                $this->db->where('id', $insert_id);
                                if ($this->db->update('tbl_game', $dataU)) {
                                    echo "<script language=\"JavaScript\">";
                                    echo "alert('เพิ่มรายชื่อเกมส์เรียบร้อยแล้ว !!');window.location='backend-game';";
                                    echo "</script>";
                                } else {
                                    echo "<script language=\"JavaScript\">";
                                    echo "alert('เกิดข้อผิดพลาดในการเพิ่มรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                                    echo "</script>";
                                }
                            }
                        }
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการเพิ่มรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                        echo "</script>";
                    }
                }
            }
        }
    }

    function game_update()
    {
        $id             = base64_decode($this->input->post('id'));
        $title          = $this->input->post('title');
        $url            = $this->input->post('url');
        $file_name      = $this->input->post('file_name');
        $file_cover     = $this->input->post('file_cover');

        if (!empty($_FILES['file_name']['name'] || $_FILES['file_cover']['name'])) {
            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/game';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|webp';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "game-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if (!empty($_FILES['file_name']['name'])) {
                if ($this->upload->do_upload('file_name')) {
                    $gamber     = $this->upload->data();

                    $data = array(
                        'title'         => $title,
                        'url'           => $url,
                        'file_name'     => $gamber['file_name'],
                        'update_at'     => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('id', $id);
                    if ($this->db->update('tbl_game', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('อัพเดทรายชื่อเกมส์เรียบร้อยแล้ว !!');window.location='backend-game';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการอัพเดทรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                        echo "</script>";
                    }
                }
            } elseif (!empty($_FILES['file_cover']['name'])) {
                if ($this->upload->do_upload('file_cover')) {
                    $gamber     = $this->upload->data();

                    $data = array(
                        'title'         => $title,
                        'url'           => $url,
                        'file_cover'    => $gamber['file_name'],
                        'update_at'     => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('id', $id);
                    if ($this->db->update('tbl_game', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('อัพเดทรายชื่อเกมส์เรียบร้อยแล้ว !!');window.location='backend-game';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการอัพเดทรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                        echo "</script>";
                    }
                }
            }
        } else {
            $data = array(
                'title'         => $title,
                'url'           => $url,
                'update_at'     => date('Y-m-d H:i:s'),
            );

            $this->db->where('id', $id);
            if ($this->db->update('tbl_game', $data)) {
                echo "<script language=\"JavaScript\">";
                echo "alert('อัพเดทรายชื่อเกมส์เรียบร้อยแล้ว !!');window.location='backend-game';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการอัพเดทรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                echo "</script>";
            }
        }
    }

    function game_delete()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $id = base64_decode($this->input->get('id'));

            $this->db->where('id', $id);
            if ($this->db->delete('tbl_game')) {
                echo "<script language=\"JavaScript\">";
                echo "alert('คุณได้ลบรายชื่อเกมส์เรียบร้อยแล้ว.');window.location='backend-game';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการลบรายชื่อเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-game';";
                echo "</script>";
            }
        }
    }
}
