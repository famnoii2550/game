<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promotion_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        $data['get_promotion']  = $this->db->order_by('create_at', 'DESC')->get('tbl_promotion')->result_array();
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $this->load->view('options/header');
            $this->load->view('promotion-list', $data);
            $this->load->view('options/footer');
        }
    }

    function promotion_add()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $title          = $this->input->post('title');
            $detail         = $this->input->post('detail');
            $url            = $this->input->post('url');
            $create_date    = date('Y-m-d');
            $create_at      = date('Y-m-d H:i:s');
            $update_at      = date('Y-m-d H:i:s');

            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/promotion';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "promotion-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_cover']['name']) {
                if ($this->upload->do_upload('file_cover')) {

                    $gamber     = $this->upload->data();
                    $data = array(
                        'title'             => $title,
                        'url'               => $url,
                        // 'detail'            => $detail,
                        'file_cover'        => $gamber['file_name'],
                        'create_date'       => $create_date,
                        'create_at'         => $create_at,
                    );
                    if ($this->db->insert('tbl_promotion', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เพิ่มโปรโมชั่นเรียบร้อยแล้ว !!');window.location='backend-promotion';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการเพิ่มโปรโมชั่น กรุณาลองใหม่อีกครั้ง !!');window.location='backend-promotion';";
                        echo "</script>";
                    }
                }
            }
        }
    }

    function upload_image()
    {
        if (isset($_FILES["image"]["name"])) {
            $config['upload_path'] = 'uploads/promotion/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image')) {
                $this->upload->display_errors();
                return FALSE;
            } else {
                $data = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'uploads/promotion/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '60%';
                $config['width'] = 800;
                $config['height'] = 533;
                $config['new_image'] = 'uploads/promotion/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'uploads/promotion/' . $data['file_name'];
            }
        }
    }

    function update_promotion()
    {
        $id             = base64_decode($this->input->post('id'));
        $title          = $this->input->post('title');
        $detail         = $this->input->post('detail');
        $url            = $this->input->post('url');
        $file_cover     = $this->input->post('file_cover');

        if (!empty($_FILES['file_cover']['name'])) {
            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/promotion';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "promotion-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_cover']['name']) {
                if ($this->upload->do_upload('file_cover')) {
                    $gamber     = $this->upload->data();

                    $data = array(
                        'title'         => $title,
                        'url'               => $url,
                        // 'detail'        => $detail,
                        'file_cover'    => $gamber['file_name'],
                        'update_at'     => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('id', $id);
                    if ($this->db->update('tbl_promotion', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('แก้ไขโปรโมชั่นเรียบร้อยแล้ว !!');window.location='backend-promotion';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการอัพเดทโปรโมชั่น กรุณาลองใหม่อีกครั้ง !!');window.location='backend-promotion';";
                        echo "</script>";
                    }
                }
            }
        } else {
            $data = array(
                'title'         => $title,
                'url'               => $url,
                // 'detail'        => $detail,
                'update_at'     => date('Y-m-d H:i:s'),
            );

            $this->db->where('id', $id);
            if ($this->db->update('tbl_promotion', $data)) {
                echo "<script language=\"JavaScript\">";
                echo "alert('แก้ไขโปรโมชั่นเรียบร้อยแล้ว !!');window.location='backend-promotion';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการอัพเดทโปรโมชั่น กรุณาลองใหม่อีกครั้ง !!');window.location='backend-promotion';";
                echo "</script>";
            }
        }
    }

    function delete_promotion()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $id = base64_decode($this->input->get('id'));

            $this->db->where('id', $id);
            if ($this->db->delete('tbl_promotion')) {
                echo "<script language=\"JavaScript\">";
                echo "alert('คุณได้ลบรายการโปรโมชั่นเรียบร้อยแล้ว.');window.location='backend-promotion';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการลบโปรโมชั่นนี้ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-promotion';";
                echo "</script>";
            }
        }
    }
}
