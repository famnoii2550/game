<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Order_model');
    }

    function index()
    {
        if (empty($this->session->userdata('username'))) {
            $this->load->view('login');
        } else {
            $data['get_order'] = $this->Order_model->get_order();
            $this->load->view('options/header');
            $this->load->view('orders-list',$data);
            $this->load->view('options/footer');
        }
    }
}
