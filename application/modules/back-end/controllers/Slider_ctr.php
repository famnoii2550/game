<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Slider_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        $data['get_slider']  = $this->db->order_by('create_at', 'DESC')->get('tbl_slider')->result_array();
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $this->load->view('options/header');
            $this->load->view('slider-list', $data);
            $this->load->view('options/footer');
        }
    }

    function slider_add()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $create_date        = date('Y-m-d');
            $create_at          = date('Y-m-d H:i:s');

            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/slider';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "slider-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_name']['name']) {
                if ($this->upload->do_upload('file_name')) {

                    $gamber     = $this->upload->data();
                    $data = array(
                        'file_name'         => $gamber['file_name'],
                        'create_date'       => $create_date,
                        'create_at'         => $create_at,
                    );
                    if ($this->db->insert('tbl_slider', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เพิ่มรูปภาพเรียบร้อยแล้ว !!');window.location='backend-slider';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการเพิ่มรูปภาพ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-slider';";
                        echo "</script>";
                    }
                }
            }
        }
    }

    function slider_update()
    {
        $id             = base64_decode($this->input->post('id'));
        $file_name      = $this->input->post('file_name');

        if (!empty($_FILES['file_name']['name'])) {
            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/slider';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|webp';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "game-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if (!empty($_FILES['file_name']['name'])) {
                if ($this->upload->do_upload('file_name')) {
                    $gamber     = $this->upload->data();

                    $data = array(
                        'file_name'     => $gamber['file_name'],
                        'update_at'     => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('id', $id);
                    if ($this->db->update('tbl_slider', $data)) {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('อัพเดทรูปภาพเรียบร้อยแล้ว !!');window.location='backend-slider';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการอัพเดทรูปภาพ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-slider';";
                        echo "</script>";
                    }
                }
            }
        }
    }
    
    function slider_delete()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $id = base64_decode($this->input->get('id'));

            $this->db->where('id', $id);
            if ($this->db->delete('tbl_slider')) {
                echo "<script language=\"JavaScript\">";
                echo "alert('คุณได้ลบรูปภาพเรียบร้อยแล้ว.');window.location='backend-slider';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการลบรูปภาพ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-slider';";
                echo "</script>";
            }
        }
    }
}
