<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        if (empty($this->session->userdata('username'))) {
            $this->load->view('login');
        } else {
            redirect('backend-game', 'refresh');
        }
    }

    function loginMe()
    {

        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $this->load->model('Login_model');

        if ($this->Login_model->login($username, $password)) {
            $user_data = array(
                'username' => $username
            );
            $this->session->set_userdata($user_data);
            redirect('backend-game');
        } else {
            $this->session->set_flashdata('error', '<div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i> กรุณากรอก Username หรือ Password ให้ถูกต้อง !! </div>');
            redirect('backend-login', 'refresh');
        }
    }

    public function logout()
    {
        $this->session->sess_destroy(); //ล้างsession

        $this->cart->destroy(); //ล้าง Cart

        redirect('backend-login'); //กลับไปหน้า Login
    }

    function re_password()
    {
        $id                 = $this->input->post('id');
        $new_pass           = md5($this->input->post('new_pass'));
        $re_pass            = $this->input->post('re_pass');
        $update_at          = date('Y-m-d H:i:s');
        $url                = $this->input->post('url');

        $data = array(
            'password'      => $new_pass,
            'update_at'     => $update_at
        );
        $this->db->where('id', $id);
        if ($this->db->update('tbl_admin', $data)) {
            echo "<script language=\"JavaScript\">";
            echo "alert('คุณได้ทำการเปลี่ยนรหัสผ่านเรียบร้อยแล้ว กรุณาเข้าสู่ระบบใหม่อีกครั้งค่ะ.');window.location='backend-logout';";
            echo "</script>";
        } else {
            echo "<script language=\"JavaScript\">";
            echo "alert('คุณได้ทำการเปลี่ยนรหัสผ่านเรียบร้อยแล้ว กรุณาเข้าสู่ระบบใหม่อีกครั้งค่ะ.');window.location='$url';";
            echo "</script>";
        }
    }
}
