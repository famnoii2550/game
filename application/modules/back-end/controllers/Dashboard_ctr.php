<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $this->load->view('options/header');
            $this->load->view('dashboard');
            $this->load->view('options/footer');
        }
    }

    function card_game()
    {
        $data['get_cardgame'] = $this->db->get('tbl_card')->result_array();
        $data['game'] = $this->db->get('tbl_game')->result_array();
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $this->load->view('options/header');
            $this->load->view('card-game', $data);
            $this->load->view('options/footer');
        }
    }

    function card_game_add()
    {

        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $title         = $this->input->post('title');
            $price         = $this->input->post('price');
            $point         = $this->input->post('point');
            $id_game         = $this->input->post('id_game');
            $create_date    = date('Y-m-d');
            $create_at      = date('Y-m-d H:i:s');
            // $update_at      = date('Y-m-d H:i:s');
            $type_point       = $this->input->post('type_point');
            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/card_game';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "card_game-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_name']['name']) {
                if ($this->upload->do_upload('file_name')) {

                    $gamber     = $this->upload->data();
                    $data = array(
                        'title'            => $title,
                        'file_name'         => $gamber['file_name'],
                        'create_date'       => $create_date,
                        'create_at'         => $create_at,
                        'type_point'        => $type_point
                    );
                    $this->db->insert('tbl_card', $data);
                    $id_card = $this->db->insert_id();

                    //card ทั้งหมด
                    foreach ($price as $key => $price) {
                    
                        $data = [
                            'id_card' => $id_card,
                            'price' => $price,
                            'point' => $point[$key],
                            'create_date' => $create_date,
                            'create_at' => $create_at
                        ];
                         $this->db->insert('tbl_card_price', $data);

                    }

                    //game ทั้งหมด
                    foreach ($id_game as $key => $id_game) {
                    
                        $data = [
                            'id_card' => $id_card,
                            'id_game' => $id_game,
                            'create_date' => $create_date,
                            'create_at' => $create_at
                        ];
                        $success = $this->db->insert('tbl_card_game', $data);

                    }

                    if ($success > 0) {

                        echo "<script language=\"JavaScript\">";
                        echo "alert('เพิ่มบัตรเติมเกมส์เรียบร้อยแล้ว !!');window.location='backend-cardgame';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการเพิ่มบัตรเติมเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-cardgame';";
                        echo "</script>";

                    }

                }
            }

            
        }
    }

    function card_game_edit()
    {

        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $id_card       = $this->input->post('id_card');
            $title         = $this->input->post('title');
            $price         = $this->input->post('price');
            $point         = $this->input->post('point');
            $id_game       = $this->input->post('id_game');
            $create_date    = date('Y-m-d');
            $create_at      = date('Y-m-d H:i:s');
            $update_at     = date('Y-m-d H:i:s');
            $type_point       = $this->input->post('type_point');
            
            $id_card_price      = $this->input->post('id_card_price');

            $this->load->library('upload');

            // |xlsx|pdf|docx
            $config['upload_path'] = 'uploads/card_game';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size']     = '200480';
            $config['max_width'] = '5000';
            $config['max_height'] = '5000';
            $config['width'] = 800;
            $config['height'] = 533;
            $name_file = "card_game-" . time();
            $config['file_name'] = $name_file;

            $this->upload->initialize($config);

            $data = array();

            if ($_FILES['file_name']['name']) {
                if ($this->upload->do_upload('file_name')) {

                    $gamber     = $this->upload->data();
                    $data = array(
                        'title'            => $title,
                        'file_name'         => $gamber['file_name'],
                        'update_at'         => $update_at,
                        'type_point'         => $type_point,
                    );
                    $this->db->where('id',$id_card);
                    $this->db->update('tbl_card', $data);


                    //card ทั้งหมด
                    $this->db->where('id_card',$id_card);
                    $this->db->delete('tbl_card_price');
                    foreach ($price as $key => $price) {
                        $data = [
                            'id_card' => $id_card,
                            'price' => $price,
                            'point' => $point[$key],
                            'create_at' => $create_at
                        ];
                        $success = $this->db->insert('tbl_card_price', $data);
                    }

                    // game ทั้งหมด
                    $this->db->where('id_card',$id_card);
                    $this->db->delete('tbl_card_game');
                    foreach ($id_game as $key => $idGame) {
                        $data = [
                            'id_card' => $id_card,
                            'id_game' => $idGame,
                            'create_date' => $create_date,
                            'create_at' => $create_at
                        ];
                        $success = $this->db->insert('tbl_card_game', $data);
                    }


                    if ($success > 0) {

                        echo "<script language=\"JavaScript\">";
                        echo "alert('เพิ่มบัตรเติมเกมส์เรียบร้อยแล้ว !!');window.location='backend-cardgame';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการเพิ่มบัตรเติมเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-cardgame';";
                        echo "</script>";

                    }

                }
            }else{
                    $data = array(
                        'title'            => $title,
                        'update_at'         => $update_at,
                        'type_point'        => $type_point
                    );
                    $this->db->where('id',$id_card);
                    $this->db->update('tbl_card', $data);

                    //card ทั้งหมด
                    $this->db->where('id_card',$id_card);
                    $this->db->delete('tbl_card_price');
                    foreach ($price as $key => $price) {
                        $data = [
                            'id_card' => $id_card,
                            'price' => $price,
                            'point' => $point[$key],
                            'create_at' => $create_at
                        ];
                        $success = $this->db->insert('tbl_card_price', $data);
                    }

                    
                    // game ทั้งหมด
                    $this->db->where('id_card',$id_card);
                    $this->db->delete('tbl_card_game');
                    foreach ($id_game as $key => $idGame) {
                        $data = [
                            'id_card' => $id_card,
                            'id_game' => $idGame,
                            'create_date' => $create_date,
                            'create_at' => $create_at
                        ];
                        $success = $this->db->insert('tbl_card_game', $data);
                    }

                    if ($success > 0) {

                        echo "<script language=\"JavaScript\">";
                        echo "alert('แก้ไขบัตรเติมเกมส์เรียบร้อยแล้ว !!');window.location='backend-cardgame';";
                        echo "</script>";
                    } else {
                        echo "<script language=\"JavaScript\">";
                        echo "alert('เกิดข้อผิดพลาดในการแก้ไขบัตรเติมเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-cardgame';";
                        echo "</script>";

                    }

                
            }

            
        }
    }

    public function card_game_delete()
    {
        if (empty($this->session->userdata('username'))) {
            redirect('backend-login');
        } else {
            $success = 0;
            $id = $this->input->get('id');
            $id = base64_decode($id);
            $this->db->where('id',$id);
            $this->db->delete('tbl_card');

            $this->db->where('id_card',$id);
            $this->db->delete('tbl_card_price');

            $this->db->where('id_card',$id);
            $this->db->delete('tbl_card_game');

            $success = 1;
            if ($success > 0) {

                echo "<script language=\"JavaScript\">";
                echo "alert('ลบบัตรเติมเกมส์เรียบร้อยแล้ว !!');window.location='backend-cardgame';";
                echo "</script>";
            } else {
                echo "<script language=\"JavaScript\">";
                echo "alert('เกิดข้อผิดพลาดในการลบบัตรเติมเกมส์ กรุณาลองใหม่อีกครั้ง !!');window.location='backend-cardgame';";
                echo "</script>";

            }
        }
    }

}
