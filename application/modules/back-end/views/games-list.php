<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 col-md-6 col-sm-12">
                <h4 class="m-t-20 m-b-30">เกมส์ทั้งหมด</h4>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary waves-effect waves-light m-t-20 m-b-30" data-toggle="modal" data-target=".bs-example-modal-center">เกมส์ทั้งหมด</button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">ตารางแสดงรายชื่อเกมส์ทั้งหมด</h4>

                        <table class="table">
                            <thead class="thead-default">
                                <tr>
                                    <th>#</th>
                                    <th>รายชื่อเกมส์</th>
                                    <th>รูปภาพปก</th>
                                    <th>รูปภาพรายละเอียด</th>
                                    <th>url</th>
                                    <th>วันที่สร้าง</th>
                                    <th>เครื่องมือ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $u = 1; ?>
                                <?php $e = 1; ?>
                                <?php $i = 1; ?>
                                <?php foreach ($game_list as $key => $data) { ?>
                                    <tr>
                                        <th scope="row"><?php echo $u++; ?></th>
                                        <td><?php echo $data['title']; ?></td>
                                        <td>
                                            <a href="uploads/game/<?php echo $data['file_name']; ?>" target="_bank"><?php echo $data['file_name']; ?></a>
                                        </td>
                                        <td>
                                            <?php if (!empty($data['file_cover'])) { ?>
                                                <a href="uploads/game/<?php echo $data['file_cover']; ?>" target="_bank"><?php echo $data['file_cover']; ?></a>
                                            <?php } else { ?>
                                                <span class="badge badge-danger">ไม่มีไฟล์รูป</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php if (empty($data['url'])) { ?>
                                                <span class="badge badge-danger">ไม่มี URL</span>
                                            <?php } else { ?>
                                                <a href="<?php echo $data['url']; ?>"><?php echo $data['url']; ?></a>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo DateThai($data['create_date']); ?></td>
                                        <td>
                                            <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-center2<?php echo $i++; ?>"><i class="fa-spin fa fa-cog"></i></button>
                                            <a href="backend-game-delete?id=<?php echo base64_encode($data['id']); ?>" OnClick="return confirm('คุณต้องการที่จะลบโปรโมชั่นนี้ ใช่หรือไม่ ??');">
                                                <button class="btn btn-danger waves-effect waves-light"><i class="ion-trash-b"></i></button>
                                            </a>
                                        </td>
                                    </tr>




                                    <div class="modal fade bs-example-modal-center2<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title mt-0">แก้ไขรายชื่อเกมส์ </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <form action="backend-game-update" method="POST" enctype="multipart/form-data">
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label>ชื่อเกมส์</label>
                                                            <input type="hidden" name="id" value="<?php echo base64_encode($data['id']); ?>">
                                                            <input type="text" class="form-control" name="title" value="<?php echo $data['title']; ?>" required />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>ลิ้งค์ <span style="color:red;">* (ถ้ามี)</span></label>
                                                            <input type="text" class="form-control" name="url" value="<?php echo $data['url']; ?>" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>ไฟล์รูปภาพปก <span style="color:red;">* ขนาดไม่เกิน(558 x 400) px</span></label>
                                                            <input type="file" class="form-control" name="file_name" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label>ไฟล์รูปภาพรายละเอียด <span style="color:red;">* ขนาดไม่เกิน(468 x 468) px</span></label>
                                                            <input type="file" class="form-control" name="file_cover" />
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                                                        <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                                                    </div>
                                                </form>
                                            </div><!-- /.modal-content -->
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->

                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- container -->
</div>
<!-- Page content Wrapper -->

<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0">เพิ่มรายชื่อเกมส์</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="backend-game-add" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>ชื่อเกมส์ <span style="color:red;">* (จำเป็น)</span></label>
                        <input type="text" class="form-control" name="title" required />
                    </div>

                    <div class="form-group">
                        <label>ลิ้งค์ <span style="color:red;">* (ถ้ามี)</span></label>
                        <input type="text" class="form-control" name="url" />
                    </div>

                    <div class="form-group">
                        <label>ไฟล์รูปภาพปก <span style="color:red;">* ขนาดไม่เกิน(558 x 400) px</span></label>
                        <input type="file" class="form-control" name="file_name" required />
                    </div>
                    <div class="form-group">
                        <label>ไฟล์รูปภาพรายละเอียด <span style="color:red;">* ขนาดไม่เกิน(468 x 468) px</span></label>
                        <input type="file" class="form-control" name="file_cover" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                    <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->