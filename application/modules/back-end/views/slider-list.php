<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-4 col-md-6 col-sm-12">
                <h4 class="m-t-20 m-b-30">Slider</h4>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary waves-effect waves-light m-t-20 m-b-30" data-toggle="modal" data-target=".bs-example-modal-lg">เพิ่ม Slider</button>
            </div>
        </div>
        <!-- end row -->
        <?php $i = 1; ?>
        <?php $e = 1; ?>
        <div class="row">
            <div class="col-12">
                <div class="card-columns">
                    <?php foreach ($get_slider as $key => $data) { ?>
                        <div class="card m-b-30">
                            <img class="card-img-top img-fluid" src="uploads/slider/<?php echo $data['file_name']; ?>" alt="Card image cap">
                            <div class="card-body">
                                <!-- <h4 class="card-title font-16 mt-0"><?php echo $data['title']; ?></h4> -->
                                <p class="card-text">
                                    <small class="text-muted"><?php echo DateThai($data['create_date']); ?></small>
                                </p>
                                <p class="card-text">

                                    <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg2<?php echo $i++; ?>"><i class="fa-spin fa fa-cog"></i></button>
                                    <a href="backend-slider-delete?id=<?php echo base64_encode($data['id']); ?>" OnClick="return confirm('คุณต้องการที่จะลบรูปภาพนี้ ใช่หรือไม่ ??');">
                                        <button class="btn btn-danger waves-effect waves-light"><i class="ion-trash-b"></i></button>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="modal fade bs-example-modal-lg2<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabelEdit" aria-hidden="true">
                            <div class="modal-dialog modal-center">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0">อัพเดท Slider</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form action="backend-slider-update" method="POST" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(1520 x 395) px</span></label>
                                                <input type="hidden" name="id" value="<?php echo base64_encode($data['id']); ?>">
                                                <input type="file" class="form-control" name="file_name" required />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                                            <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                                        </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- end row -->



        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">เพิ่ม Slider</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form action="backend-slider-add" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(1520 x 395) px</span></label>
                                <input type="file" class="form-control" name="file_name" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                            <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div><!-- container -->


</div> <!-- Page content Wrapper -->