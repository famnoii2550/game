<div class="page-content-wrapper ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">

                        <h4 class="mt-0 header-title">ตารางคำสั่งซื้อ</h4>

                        <table id="datatable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>เลขที่ออเดอร์</th>
                                    <th>บัตรเติมเงิน</th>
                                    <th>จำนวน</th>
                                    <th>ราคารวม</th>
                                    <!-- <th>รหัส TOKEN</th> -->
                                    <th>สั่งซื้อเมื่อ</th>
                                </tr>
                            </thead>


                            <tbody>
                                <?php foreach ($get_order as $key => $data) { ?>
                                    <?php $get_card = $this->db->get_where('tbl_card', ['id' => $data['id_card']])->row_array(); ?>
                                    <tr>
                                        <td><?php echo $data['order_number']; ?></td>
                                        <td><?php echo $get_card['title']; ?></td>
                                        <td><?php echo $data['count']; ?></td>
                                        <td><?php echo $data['total']; ?></td>
                                        <!-- <td></td> -->
                                        <td><?php echo DateThai($data['create_at']); ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div> <!-- end col -->
        </div> <!-- end row -->

    </div><!-- container -->


</div> <!-- Page content Wrapper -->