<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-4 col-md-6 col-sm-12">
                <h4 class="m-t-20 m-b-30">บัตรเติมเกมส์</h4>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary waves-effect waves-light m-t-20 m-b-30" data-toggle="modal" data-target=".bs-example-modal-center">เพิ่มบัตรเติมเกมส์</button>
            </div>
        </div>
        <!-- end row -->
        <?php $i = 1; ?>
        <?php $e = 1; ?>
        <div class="row">
            <div class="col-12">
                <div class="card-columns">
                    <?php foreach ($get_cardgame as $key => $cardgame) {
                        $card_price = $this->db->get_where('tbl_card_price', ['id_card' => $cardgame['id']])->result_array();
                    ?>
                        <div class="card m-b-30">
                            <img class="card-img-top img-fluid" src="uploads/card_game/<?php echo $cardgame['file_name']; ?>" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title font-20 mt-0"><?php echo $cardgame['title']; ?></h4>
                                <p class="card-text">
                                    <small class="text-muted"><?php echo DateThai($cardgame['create_date']); ?></small>
                                </p>
                                <p class="card-text">

                                    <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg2<?php echo $i++; ?>"><i class="fa-spin fa fa-cog"></i></button>
                                    <a href="backend-card-game-delete?id=<?php echo base64_encode($cardgame['id']); ?>" OnClick="return confirm('คุณต้องการที่จะลบโปรโมชั่นนี้ ใช่หรือไม่ ??');">
                                        <button class="btn btn-danger waves-effect waves-light"><i class="ion-trash-b"></i></button>
                                    </a>
                                </p>
                            </div>
                        </div>


                        <div class="modal fade bs-example-modal-lg2<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabelEdit" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0">แก้ไขบัตรเติมเกมส์</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form action="backend-card-game-edit" id="mypromotion" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="id_card" value="<?php echo $cardgame['id']; ?>">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>หัวข้อเรื่อง</label>
                                                <input type="text" class="form-control" name="title" required value="<?php echo $cardgame['title']; ?>" />
                                            </div>
                                            <?php
                                            foreach ($card_price as $key => $cardPrice) {

                                            ?>
                                                <?php if ($key == 0) {  ?>

                                                    <div id="plus_price_edit">

                                                    <?php } else { ?>

                                                        <div class="new_price_edit">

                                                        <?php } ?>
                                                        <div class="row">
                                                            <?php if ($key != 0) { ?>
                                                                <div class="col-12 text-right form-group"><button type="button" id="remove_price_edit" class="btn btn-danger"><i class="fa fa-minus"></i></button></div>
                                                            <?php } ?>
                                                            <input type="hidden" name="id_card_price[]" value="<?php echo $cardPrice['id']; ?>">
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label>ราคาบัตรเติมเกมส์</label>
                                                                    <div class="input-group">
                                                                        <input type="number" class="form-control" name="price[]" value="<?php echo $cardPrice['price']; ?>">
                                                                        <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-cash-multiple"></i></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label>POINT ที่จะได้</label>
                                                                    <div class="input-group">
                                                                        <input type="number" class="form-control" name="point[]" value="<?php echo $cardPrice['point']; ?>">
                                                                        <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-coin"></i></span></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="new_price_edit_last"></div>
                                                    <div style="text-align:center;">
                                                        <button type="button" id="btn2edit" class="btn btn-success" title="เพิ่มราคาบัตรเติมเกมส์"><i class="fa fa-plus"></i></button>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>ค่าเงินสกุล POINT <span style="color:red;">*</span></label>
                                                        <input type="text" class="form-control" name="type_point" required value="<?php echo $cardgame['type_point']; ?>" />
                                                    </div>

                                                    <div class="form-group">
                                                        <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(264 x 220) px</span></label>
                                                        <input type="file" class="form-control" name="file_name" onchange="readURL_edit<?php echo $cardgame['id']; ?>(this);" />
                                                        <div style="width: 115px;margin: 15px auto 0;">
                                                            <img id="blah_edit<?php echo $cardgame['id']; ?>" style="max-width:100%;" src="uploads/card_game/<?php echo $cardgame['file_name']; ?>" alt="" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div>
                                                            <label>เกมส์ที่สามารถใช้งานได้</label>
                                                        </div>

                                                        <?php foreach ($game as $gameDetail) { ?>
                                                            <div class="form-check-inline">
                                                                <label class="form-check-label">
                                                                    <?php $cardgameGame = $this->db->get_where('tbl_card_game', ['id_card' => $cardgame['id'], 'id_game' => $gameDetail['id']])->row_array(); ?>

                                                                    <input type="checkbox" class="form-check-input" name="id_game[]" value="<?php echo $gameDetail['id']; ?>" <?php if (!empty($cardgameGame)) {
                                                                                                                                                                                    echo "checked";
                                                                                                                                                                                } ?>><?php echo $gameDetail['title']; ?>

                                                                </label>
                                                            </div>

                                                        <?php } ?>
                                                        <div class="btn btn-success active_game" style="display:none;">แก้ไขเรียบร้อยแล้ว</div>
                                                    </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                                                        <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                                                    </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        <script type="text/javascript">
                            function readURL_edit<?php echo $cardgame['id']; ?>(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();

                                    reader.onload = function(e) {
                                        $('#blah_edit<?php echo $cardgame['id']; ?>').attr('src', e.target.result);
                                    }

                                    reader.readAsDataURL(input.files[0]);
                                }
                            }
                        </script>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- end row -->



        <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">เพิ่มบัตรเติมเกมส์</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form action="backend-card-game-add" id="mypromotion" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>หัวข้อเรื่อง</label>
                                <input type="text" class="form-control" name="title" required />
                            </div>
                            <div id="plus_price">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>ราคาบัตรเติมเกมส์</label>
                                            <div class="input-group">
                                                <input type="number" class="form-control" name="price[]">
                                                <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-cash-multiple"></i></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label>POINT ที่จะได้</label>
                                            <div class="input-group">
                                                <input type="number" class="form-control" name="point[]">
                                                <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-coin"></i></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>

                            <div class="form-group" style="text-align:center;">
                                <button type="button" id="btn2" class="btn btn-success" title="เพิ่มราคาบัตรเติมเกมส์"><i class="fa fa-plus"></i></button>
                            </div>

                            <div class="form-group">
                                <label>ค่าเงินสกุล POINT <span style="color:red;">*</span></label>
                                <input type="text" class="form-control" name="type_point" required />
                            </div>

                            <div class="form-group">
                                <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(264 x 220) px</span></label>
                                <input type="file" class="form-control" name="file_name" onchange="readURL(this);" required />
                                <div style="width: 115px;margin: 15px auto 0;">
                                    <img id="blah" style="max-width:100%;" src="" alt="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div>
                                    <label>เกมส์ที่สามารถใช้งานได้</label>
                                </div>

                                <?php foreach ($game as $key => $gameDetail) { ?>
                                    <div class="form-check-inline">
                                        <label class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="id_game[]" value="<?php echo $gameDetail['id']; ?>"><?php echo $gameDetail['title']; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                            <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div><!-- container -->


</div> <!-- Page content Wrapper -->

<script type="text/javascript">
    $(document).ready(function() {

        $("#btn2").click(function() {
            $('.modal .modal-dialog .modal-content form .modal-body #plus_price').append('<?php $this->load->view('options/plus_price'); ?>');
        });
        $("body").on("click", "#remove_price", function(e) {
            $(this).parents('.new_price').remove();
        });

        $("#btn2edit").click(function() {
            $('.modal .modal-dialog .modal-content form .new_price_edit_last').append('<?php $this->load->view('options/plus_price'); ?>');
        });
        $("body").on("click", "#remove_price_edit", function(e) {
            $(this).parents('.new_price_edit').remove();
        });
    });
</script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>