<!-- jQuery  -->
<script src="public/frontend/assets/js/ajax.js"></script>
<script src="public/backend/assets/js/jquery.min.js"></script>
<script src="public/backend/assets/js/popper.min.js"></script>
<script src="public/backend/assets/js/bootstrap.min.js"></script>
<script src="public/backend/assets/js/modernizr.min.js"></script>
<script src="public/backend/assets/js/detect.js"></script>
<script src="public/backend/assets/js/fastclick.js"></script>
<script src="public/backend/assets/js/jquery.slimscroll.js"></script>
<script src="public/backend/assets/js/jquery.blockUI.js"></script>
<script src="public/backend/assets/js/waves.js"></script>
<script src="public/backend/assets/js/jquery.nicescroll.js"></script>
<script src="public/backend/assets/js/jquery.scrollTo.min.js"></script>
<!-- Required datatable js -->
<script src="public/backend/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="public/backend/assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Buttons examples -->
<script src="public/backend/assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="public/backend/assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="public/backend/assets/plugins/datatables/jszip.min.js"></script>
<script src="public/backend/assets/plugins/datatables/pdfmake.min.js"></script>
<script src="public/backend/assets/plugins/datatables/vfs_fonts.js"></script>
<script src="public/backend/assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="public/backend/assets/plugins/datatables/buttons.print.min.js"></script>
<script src="public/backend/assets/plugins/datatables/buttons.colVis.min.js"></script>
<!-- Responsive examples -->
<script src="public/backend/assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="public/backend/assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Datatable init js -->
<script src="public/backend/assets/pages/datatables.init.js"></script>
<!--Morris Chart-->
<script src="public/backend/assets/plugins/morris/morris.min.js"></script>
<script src="public/backend/assets/plugins/raphael/raphael-min.js"></script>

<script src="public/backend/assets/pages/dashborad.js"></script>
<!--Wysiwig js-->
<script src="public/backend/assets/plugins/tinymce/tinymce.min.js"></script>
<!-- App js -->
<script src="public/backend/assets/js/app.js"></script>

<script>
    $(document).ready(function() {
        if ($("#elm1").length > 0) {
            tinymce.init({
                selector: "textarea#elm1",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [{
                        title: 'Bold text',
                        inline: 'b'
                    },
                    {
                        title: 'Red text',
                        inline: 'span',
                        styles: {
                            color: '#ff0000'
                        }
                    },
                    {
                        title: 'Red header',
                        block: 'h1',
                        styles: {
                            color: '#ff0000'
                        }
                    },
                    {
                        title: 'Example 1',
                        inline: 'span',
                        classes: 'example1'
                    },
                    {
                        title: 'Example 2',
                        inline: 'span',
                        classes: 'example2'
                    },
                    {
                        title: 'Table styles'
                    },
                    {
                        title: 'Table row 1',
                        selector: 'tr',
                        classes: 'tablerow1'
                    }
                ]
            });
        }
    });
</script>

</body>

</html>