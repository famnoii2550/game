<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>24BUYM | จำหน่าย บัตรเติมเกมออนไลน์ ราคาถูก ประหยัด เติมง่าย ซื้อ-ขายไอดีเกมออนไลน์ บัตรเติมเกม ไอเทมโค้ตเกม เติมเงินมือถือ เกมมิ่งเกียร์ คอมพิวเตอร์ ตลาดซื้อขายออนไลน์ ปั้มเกม รวมไปถึง ระบบสุ่มรางวัล มากมาย ขายไอดีเกม จำหน่ายบัตรเติมเกมออนไลน์ ราคาถูก 24ชั่วโมง</title>
    <meta content="24BUYM |ซื้อ-ขายไอดีเกมออนไลน์ บัตรเติมเกม ไอเทมโค้ตเกม เติมเงินมือถือ เกมมิ่งเกียร์ คอมพิวเตอร์ ตลาดซื้อขายออนไลน์ ปั้มเกม รวมไปถึง ระบบสุ่มรางวัล มากมาย ขายไอดีเกม จำหน่ายบัตรเติมเกมออนไลน์ ราคาถูก 24ชั่วโมง" name="description" />
    <meta content="24BUYM |ซื้อ-ขายไอดีเกมออนไลน์ บัตรเติมเกม ไอเทมโค้ตเกม เติมเงินมือถือ เกมมิ่งเกียร์ คอมพิวเตอร์ ตลาดซื้อขายออนไลน์ ปั้มเกม รวมไปถึง ระบบสุ่มรางวัล มากมาย ขายไอดีเกม จำหน่ายบัตรเติมเกมออนไลน์ ราคาถูก 24ชั่วโมง" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="shortcut icon" href="public/frontend/assets/img/large-1568709940461.png">
    <!-- DataTables -->
    <link href="assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="public/backend/assets/plugins/morris/morris.css">

    <link href="public/backend/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="public/backend/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="public/backend/assets/css/style.css" rel="stylesheet" type="text/css">

</head>
<?php $session = $this->db->get_where('tbl_admin', ['username' => $this->session->userdata('username')])->row_array(); ?>
<?php
function DateThai($strDate)
{
    $strYear = date("Y", strtotime($strDate)) + 543;
    $strMonth = date("n", strtotime($strDate));
    $strDay = date("j", strtotime($strDate));
    $strHour = date("H", strtotime($strDate));
    $strMinute = date("i", strtotime($strDate));
    $strSeconds = date("s", strtotime($strDate));
    $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
    $strMonthThai = $strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear";
}; ?>
<script src="public/backend/assets/js/ajax.js"></script>

<body class="fixed-left">

    <!-- Loader -->
    <div id="preloader">
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                <i class="ion-close"></i>
            </button>

            <!-- LOGO -->
            <div class="topbar-left">
                <div class="text-center">
                    <!--<a href="index.html" class="logo">Admiry</a>-->
                    <a href="<?php echo $this->uri->segment(1); ?>" class="logo"><img src="public/frontend/assets/img/large-1568709940461.png" alt="logo"></a>
                </div>
            </div>

            <div class="sidebar-inner slimscrollleft">

                <div id="sidebar-menu">
                    <ul>
                        <li class="menu-title">Menu</li>
                        <li>
                            <a href="backend-slider" class="waves-effect"><i class="fa fa-picture-o"></i><span> ภาพสไลด์ </span></a>
                        </li>
                        <li>
                            <a href="backend-game" class="waves-effect"><i class="fa fa-archive"></i><span> เกมส์ทั้งหมด </span></a>
                        </li>
                        <li>
                            <a href="backend-cardgame" class="waves-effect"><i class="fa fa-money"></i><span> บัตรเติมเกมส์ </span></a>
                        </li>
                        <li>
                            <a href="backend-promotion" class="waves-effect"><i class="ti-announcement"></i><span> โปรโมชั่น </span></a>
                        </li>
                        <li>
                            <a href="backend-order" class="waves-effect"><i class="fa fa-shopping-cart"></i><span> ออเดอร์ </span></a>
                        </li>
                        <li>
                            <a href="backend-logout" class="waves-effect" style="color:red;" onclick="return confirm('Are you sure you want to log out ??')"><i class="fa fa-sign-out"></i><span> ออกจากระบบ </span></a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div> <!-- end sidebarinner -->
        </div>
        <!-- Left Sidebar End -->

        <!-- Start right Content here -->

        <div class="content-page">
            <!-- Start content -->
            <div class="content">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-inline float-right mb-0">

                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="public/backend/assets/images/users/Admin-icon.png" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <a class="dropdown-item" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="mdi mdi-settings m-r-5 text-muted"></i> Settings</a>
                                    <a class="dropdown-item" href="backend-logout" onclick="return confirm('Are you sure you want to log out ??')"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="list-inline-item">
                                <button type="button" class="button-menu-mobile open-left waves-effect">
                                    <i class="ion-navicon"></i>
                                </button>
                            </li>

                        </ul>

                        <div class="clearfix"></div>

                    </nav>

                </div>
                <!-- Top Bar End -->

                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title mt-0">เปลี่ยนรหัสผ่าน</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <form action="backend-repass" method="POST" enctype="multipart/form-data" onSubmit="return checkPassword(this)">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>รหัสผ่านใหม่</label>
                                        <input type="hidden" name="id" value="<?php echo $session['id']; ?>">
                                        <input type="hidden" name="url" value="<?php echo $this->uri->segment(1); ?>">
                                        <input type="password" class="form-control" name="new_pass" id="password" required />
                                    </div>
                                    <div class="form-group">
                                        <label>พิมพ์รหัสผ่านใหม่อีกครั้ง</label>
                                        <input type="password" class="form-control" name="re_pass" id="c_password" required />
                                        <span id="message"></span>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                                    <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                                </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <script>
                    $('#password, #c_password').on('keyup', function() {
                        if ($('#password').val() == $('#c_password').val()) {
                            $('#message').html('Matching').css('color', 'green');
                        } else
                            $('#message').html('Not Matching').css('color', 'red');
                    });
                </script>

                <script>
                    // Function เพื่อตรวจสอบรหัสผ่านว่าตรงกันหรือไม่
                    function checkPassword(form) {

                        // ถ้าช่่องรหัสผ่านไม่ถูกกรอก
                        if ($('#password').val() == '')
                            alert("Please enter Password");

                        // ถ้าช่่องยืนยันรหัสผ่านไม่ถูกกรอก
                        else if ($('#c_password').val() == '')
                            alert("Please enter confirm password");

                        //ถ้าทั้งสองช่องตรงกัน  return true
                        else {
                            return true;
                        }
                    }
                </script>