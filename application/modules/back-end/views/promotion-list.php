<div class="page-content-wrapper ">

    <div class="container-fluid">

        <div class="row">
            <div class="col-4 col-md-6 col-sm-12">
                <h4 class="m-t-20 m-b-30">Promotion</h4>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary waves-effect waves-light m-t-20 m-b-30" data-toggle="modal" data-target=".bs-example-modal-lg">เพิ่มโปรโมชั่น</button>
            </div>
        </div>
        <!-- end row -->
        <?php $i = 1; ?>
        <?php $e = 1; ?>
        <div class="row">
            <div class="col-12">
                <div class="card-columns">
                    <?php foreach ($get_promotion as $key => $data) { ?>
                        <div class="card m-b-30">
                            <img class="card-img-top img-fluid" src="uploads/promotion/<?php echo $data['file_cover']; ?>" alt="Card image cap">
                            <div class="card-body">
                                <h4 class="card-title font-16 mt-0"><?php echo $data['title']; ?></h4>
                                <p class="card-text">
                                    <small class="text-muted"><?php echo DateThai($data['create_date']); ?></small>
                                </p>
                                <p class="card-text">

                                    <button type="button" class="btn btn-warning waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg2<?php echo $i++; ?>"><i class="fa-spin fa fa-cog"></i></button>
                                    <a href="backend-promotion-delete?id=<?php echo base64_encode($data['id']); ?>" OnClick="return confirm('คุณต้องการที่จะลบโปรโมชั่นนี้ ใช่หรือไม่ ??');">
                                        <button class="btn btn-danger waves-effect waves-light"><i class="ion-trash-b"></i></button>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="modal fade bs-example-modal-lg2<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabelEdit" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title mt-0">อัพเดทโปรโมชั่น</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <form action="backend-promotion-update" id="mypromotion" method="POST" enctype="multipart/form-data">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>หัวข้อเรื่อง</label>
                                                <input type="hidden" name="id" value="<?php echo base64_encode($data['id']); ?>">
                                                <input type="text" class="form-control" value="<?php echo $data['title']; ?>" name="title" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ลิ้งค์ url</label>
                                                <input type="text" class="form-control" name="url" value="<?php echo $data['url']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(362 x 204) px</span></label>
                                                <input type="file" class="form-control" name="file_cover" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                                            <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                                        </div>
                                    </form>
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- end row -->



        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title mt-0">เพิ่มโปรโมชั่น</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form action="backend-promotion-add" id="mypromotion" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>หัวข้อเรื่อง</label>
                                <input type="text" class="form-control" name="title" required />
                            </div>
                            <div class="form-group">
                                <label>ลิ้งค์ url</label>
                                <input type="text" class="form-control" name="url" required />
                            </div>
                            <div class="form-group">
                                <label>ไฟล์รูปภาพ <span style="color:red;">* ขนาดไม่เกิน(362 x 204) px</span></label>
                                <input type="file" class="form-control" name="file_cover" required />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> ยกเลิก</button>
                            <button type="submit" id="log-attendance" class="btn btn-success waves-effect waves-light"><i class="fa fa-save"></i> บันทึก</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div><!-- container -->


</div> <!-- Page content Wrapper -->

<script>
    $(function() {
        $('#mypromotion').submit(function(e) {
            e.preventDefault();
            //var formdata = $(this).serialize();
            $.ajax({
                url: 'backend-promotion-add',
                method: 'POST',
                data: {
                    title: <?php echo $this->input->post('title'); ?>,
                    detail: <?php echo $this->input->post('detail'); ?>,
                },
                success: function(res) {
                    alert(res);
                }
            });
        });

    });

    jQuery(document).ready(function() {
        $('#elm1').summernote({
            height: 300, // set editor height
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                },
                onMediaDelete: function(target) {
                    deleteImage(target[0].src);
                }
            },

        });

        function uploadImage(image) {
            var data = new FormData();
            data.append("image", image);
            $.ajax({
                url: 'backend-uploads-file',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "POST",
                success: function(url) {
                    $('#elm1').summernote("insertImage", url);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    });
</script>