<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Card_game_ctr extends CI_Controller {

	public function __construct(){

    	parent::__construct();
    	$this->load->helper('url');

  	}

    public function index()
    {
        $this->load->view('options/header');
        $this->load->view('card_game');
        $this->load->view('options/footer');
    }

    public function card_game_detail()
    {
        $id = $this->input->get('id');
        $id = base64_decode($id);
        $data['cardDetail'] = $this->db->get_where('tbl_card',['id'=>$id])->row_array();
        $data['cardDetail_price'] = $this->db->get_where('tbl_card_price',['id_card'=>$id])->result_array();
        $data['cardDetail_game'] = $this->db->get_where('tbl_card_game',['id_card'=>$id])->result_array();

        $this->load->view('options/header');
        $this->load->view('card_game_detail',$data);
        $this->load->view('options/footer');
    }

    public function select_card_game()
    {

        $price = $this->input->get('price');
        $count = $this->input->get('count');
        if ($count == "") {
            $count = "0";
        }
        $id_card = $this->input->get('id_card');
        $sum_result = $price * $count;
        $shell = 0;
        $point = $this->db->get_where('tbl_card_price',['id_card'=>$id_card,'price'=>$price])->row_array();
        $shell = $point['point'];

        
        $sum_shell_result = $shell * $count;

        $result = [];
        $result['select_price'] = $price.' บาท';
        $result['count'] = $count;
        $result['sum_result'] = $sum_result.' บาท';
        $result['sum_shell_result'] = $sum_shell_result;
        echo json_encode($result);
    }

    public function select_count_game()
    {
        $price = $this->input->get('price');
        $count = $this->input->get('count');
        if ($count == "") {
            $count = "0";
        }
        $id_card = $this->input->get('id_card');
        $sum_result = $price * $count;
        $shell = 0;
        $point = $this->db->get_where('tbl_card_price',['id_card'=>$id_card,'price'=>$price])->row_array();
        $shell = $point['point'];

        $sum_shell_result = $shell * $count;

        $result = [];
        $result['select_price'] = $price.' บาท';
        $result['count'] = $count;
        $result['sum_result'] = $sum_result.' บาท';
        $result['sum_shell_result'] = $sum_shell_result;
        echo json_encode($result);
    }

    public function order_card()
    {
        $price = $this->input->post('price');
        $count = $this->input->post('count');
        $total = $this->input->post('total');
        $totalCut = explode(" ",$total);
        $id_card = $this->input->post('id_card');
        $success = "";

        if ($totalCut[0] <= 0) {
            $success = "กรุณาเลือกบัตรเติมเกมส์และจำนวนบัตรเติมเกมส์ด้วยค่ะ";
            $result = [];
            $result['successfully'] = false;
            $result['message'] = $success;   
        }elseif($totalCut[0] > 0){
            $success = "ขอบคุณค่ะ ท่านได้ทำการทำการสั่งซื้อบัตรเติมเกมส์เรียบร้อย กรุณากดปุ่ม Copy เพื่อนำ 24beym ไปส่งให้ทีมงานใน Messenger";
            $order = "OD-".date("YmdHis").rand(1000,10000);
            // $token = openssl_random_pseudo_bytes(30);
            // $token = bin2hex($token);
            $card = $this->db->get_where('tbl_card',['id' => $id_card])->row_array();
            $token = "ต้องการบัตร ".$card['title']." จำนวน ".$count." ใบ ราคารวมทั้งหมด ".$total;
            $data = [
                "order_number"      => $order,
                "id_card"           => $id_card,
                "count"             => $count,
                "total"             => $totalCut[0],
                //"token"             => $token,
                "create_date"       => date("Y-m-d"),
                "create_at"         => date("Y-m-d H:i:s"),
            ];

            $this->db->insert('tbl_order',$data);

            $result = [];
            $result['successfully'] = true;
            $result['message'] = $success;
            $result['order'] = $order;
            $result['card_name'] = $card['title'];
            $result['card_count'] = $count;
            $result['card_price'] = $price;
            $result['card_total'] = $totalCut['0'];
            $result['token'] = $token;
        }
        
        echo json_encode($result);
    }
}
