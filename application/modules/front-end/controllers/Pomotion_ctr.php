<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pomotion_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    function index()
    {
        $data['get_promo'] = $this->db->order_by('create_at','DESC')->get('tbl_promotion')->result_array();
        $this->load->view('options/header');
        $this->load->view('pomotion', $data);
        $this->load->view('options/footer');
    }
}
