<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Topup_game_ctr extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        $this->load->helper('url');
    }

    public function index()
    {
        $data['get_game'] = $this->db->order_by('create_at','DESC')->get('tbl_game')->result_array();

        $this->load->view('options/header');
        $this->load->view('top_up_game', $data);
        $this->load->view('options/footer');
    }
}
