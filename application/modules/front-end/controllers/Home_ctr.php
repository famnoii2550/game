<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_ctr extends CI_Controller {

	public function __construct(){

    	parent::__construct();
    	$this->load->helper('url');

  	}

    function index()
    {
        $this->load->view('options/header');
        $this->load->view('index');
        $this->load->view('options/footer');
    }

    public function test()
    {
        $this->load->view('test');
    }

}
