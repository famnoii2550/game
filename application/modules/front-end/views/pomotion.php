<?php $get_promotion = $this->db->get('tbl_promotion')->result_array(); ?>
<!-- Latest news section -->
<div class="latest-news-section">
    <div class="ln-title">โปรโมชั่นสำหรับคุณ</div>
    <div class="news-ticker">
        <div class="news-ticker-contant">
            <?php foreach ($get_promotion as $key => $get_promotion) { ?>
                <div class="nt-item"><span class="new">โปรโมชั่น</span><a href="pomotion" style="color:#fff;"><?php echo $get_promotion['title']; ?>. </a></div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Latest news section end -->

<!-- Page info section -->
<section class="page-info-section set-bg" data-setbg="public/frontend/assets/img/review-bg-2.jpg">
    <div class="pi-content">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 text-center">
                    <h2 style="color:orange">โปรโมชั่น</h2>
                    <p>
                        <a href="index" style="color:#ffb320;">หน้าหลัก</a><span style="color:#fff;"> > โปรโมชั่น</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page info section -->

<!-- Page section -->
<section class="page-section recent-game-page spad" style="background-color:rgb(19, 19, 19);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <?php foreach ($get_promo as $key => $data) { ?>
                        <div class="col-md-4">
                            <div class="recent-game-item">
                                <a href="<?php echo $data['url']; ?>" target="_bank">
                                    <div class="rgi-thumb set-bg" data-setbg="uploads/promotion/<?php echo $data['file_cover']; ?>">
                                        <div class="cata new">โปรโมชั่น</div>
                                    </div>
                                </a>
                                <div class="rgi-content">
                                    <a href="<?php echo $data['url']; ?>" target="_bank">
                                        <h5 style="font-size:16px;"><?php echo $data['title']; ?></h5>
                                    </a>
                                    <p>
                                        <?php echo substr($data['detail'], 0, 50); ?>
                                    </p>
                                    <p class="comment">โพสเมื่อ : <?php echo DateThai($data['create_date']); ?></a>
                                        <!-- <div class="rgi-extra">
                                            <div class="rgi-star">
                                                <img src="public/frontend/assets/img/icons/star.png" alt="" />
                                            </div>
                                            <div class="rgi-heart">
                                                <img src="public/frontend/assets/img/icons/heart.png" alt="" />
                                            </div>
                                        </div> -->
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <!-- <div class="site-pagination">
                    <span class="active">01.</span>
                    <a style="color:#fff;" href="#">02.</a>
                    <a style="color:#fff;" href="#">03.</a>
                </div> -->
            </div>
        </div>
    </div>
</section>
<!-- Page section end -->