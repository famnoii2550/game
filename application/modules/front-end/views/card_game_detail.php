<!-- Page info section -->
<section class="page-info-section set-bg" data-setbg="public/frontend/assets/img/review-bg-2.jpg">
    <div class="pi-content">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 text-center">
                    <h2 style="color:orange">บัตรเติมเกมส์</h2>
                    <p>
                        <a href="index" style="color:#ffb320;">หน้าหลัก</a><span style="color:#fff;"> > บัตรเติมเกมส์</span> <span style="color:#fff;"> > รายละเอียด</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Page info section -->

<!-- Review section -->
<section class="review-section review-dark spad set-bg" style="background-color:rgb(19, 19, 19);">
    <div class="container">
        <div class="row text-white">
            <div class="col-lg-2 col-md-12">
                <h4 class="h_hidden">เกมส์ที่เติมได้</h4>

                <div class="col-lg-2 col-md-12 b_hidden">
                    <div class="dropdown">
                        <button class="btn btn-warning dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width:100%;font-size:16px;">เกมส์ที่เติมได้</button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"  style="width:100%;font-size:16px;">
                        <?php foreach ($cardDetail_game as $cardGame_mobile) {
                                $gameMobile = $this->db->get_where('tbl_game', ['id' => $cardGame_mobile['id_game']])->row_array();
                        ?>
                            <div class="dropdown-item" href=""><?php echo $gameMobile['title']; ?></div>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                
                <?php
                $i = 0;
                foreach ($cardDetail_game as $cardGame) {
                    $i += 1;
                    $game = $this->db->get_where('tbl_game', ['id' => $cardGame['id_game']])->row_array();
                ?>
                   
                    <!-- <div class="review-item card_game_detail_list form-group" style="cursor:pointer;" data-toggle="modal" data-target="#exampleModalScrollable<?php echo $game['id']; ?>">
                        <div class="review-cover set-bg img_card_game" style="background-position: center;color:#000; height:65%; width:100%" data-setbg="uploads/game/<?php echo $game['file_name']; ?>"></div>
                    </div> -->
                        <div class="review-item card_game_detail_list form-group" style="cursor:pointer; float: left; width: 50%;" data-toggle="modal" data-target="#exampleModalScrollable<?php echo $game['id']; ?>">
                            <img src="uploads/game/<?php echo $game['file_name']; ?>" alt="">
                        </div>

                   
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalScrollable<?php echo $game['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                            <div class="modal-content12">
                                <div class="modal-header" style="border-bottom:0">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true" style="color:#fff;font-size:30px;">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <?php if (empty($game['url'])) { ?>
                                        <img src="uploads/game/<?php echo $game['file_cover']; ?>" alt="">
                                    <?php } else { ?>
                                        <a href="<?php echo $game['url']; ?>" target="_blank">
                                            <img src="uploads/game/<?php echo $game['file_cover']; ?>" alt="">
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="col-lg-5 col-md-6">
                <div class="review-item">
                    <div class="review-cover set-bg" style="background-size:100%; height: 345px" data-setbg="uploads/card_game/<?php echo $cardDetail['file_name']; ?>">
                    </div>
                    <div class="review-text">
                        <h5><?php echo $cardDetail['title']; ?></h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-md-6">
                <div class="review-item price_card">
                    <form style="display:inherit;">
                        <div class="price_list">
                            <h4 style="font-size:16px;">ราคาบัตรทั้งหมด</h4>
                            <?php foreach ($cardDetail_price as $cardPrice) { ?>
                                <div class="price_detail">
                                    <button type="button" class="btn btn-light" value="<?php echo $cardPrice['price']; ?>" onClick="select_card(this);"><?php echo $cardPrice['price']; ?> บาท</button>
                                </div>
                            <?php } ?>

                        </div>
                        <div class="result_list">
                            <h4 style="font-size:16px;">ราคาบัตรที่เลือก</h4>
                            <div class="result_detail">
                                <div class="result_detail_list select_price btn btn-secondary">0 บาท</div>
                            </div>
                            <h4 style="font-size:16px;">จำนวน</h4>
                            <div class="result_detail">
                                <input type="number" class="result_detail_list btn btn-light" id="count" min="1" max="100" value="1" onKeyUp="select_count(this);" onClick="select_count(this);">
                            </div>
                            <h4 style="font-size:16px;">ยอดรวม</h4>
                            <div class="result_detail">
                                <div class="result_detail_list btn btn-secondary" id="sum_result">0 บาท</div>
                            </div>
                            <h4 style="font-size:16px;"><?php echo $cardDetail['type_point']; ?> ที่จะได้รับ</h4>
                            <div class="result_detail">
                                <div class="result_detail_list btn btn-secondary" id="sum_shell_result">0</div>
                            </div>

                            <input type="hidden" name="price">
                            <input type="hidden" name="count" value="1">
                            <input type="hidden" name="sum_result">
                            <input type="hidden" name="sum_shell_result">

                            <div class="result_detail">
                                <button type="button" class="result_detail_list btn btn-success" onClick="successOrder();">ยืนยันการสั่งซื้อ</button>
                            </div>

                            <div class="result_detail">
                                <button type="button" class="result_detail_list btn btn-danger">วิธีเติมเกมส์</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</section>
<!-- Review section end -->


<!-- Modal -->
<div class="modal fade bs-example-modal-center2" id="order" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title mt-0">รายการสั่งซื้อ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                                                
            <div class="modal-body">
                <div id="message_order" class="form-group"></div>
                <div class="form-group order_data_name">
                    รหัสรายการสั่งซื้อ : <div id="order_data"></div>
                </div>

                <div class="form-group">
                <table class="table table-bordered table_order">
                    <thead>
                        <tr>
                            <th scope="col">บัตร</th>
                            <th scope="col">ราคา</th>
                            <th scope="col">จำนวน</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="card_name"></td>
                            <td id="card_price"></td>
                            <td id="card_count"></td>
                        </tr>
                        <tr>
                            <th scope="col" colspan="2" style="text-align:right;">ยอดรวม</th>
                            <td id="card_total"></td>
                        </tr>
                    </tbody>
                </table>
                </div>

                <input type="text" id="token" class="form-group form-control" readonly>
                <div class="text-center text_24beym" style="font-size: 18px; color:red;">**คลิกเพื่อ Copy คำพูดนี้ส่งไปที่ข้อความแฟนเพจ Facebook**</div>
                <div class="text-center text_24beym">Click</div>
                <div class="text-center form-group text_24beym_arrow"><i class="fa fa-arrow-down" aria-hidden="true"></i></div>
                <button type="button" class="btn btn-success" id="copy_token" onClick="copyToken();">Copy</button>
            </div>
            <div class="modal-footer">

            </div>
                                                
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    function select_card(button) {
        var price_get = button.value;
        count_get = $("#count").val();

        $.ajax({
            url: "selectCard_game",
            data: {
                price: price_get,
                count: count_get,
                id_card: <?php echo $cardDetail['id']; ?>,
            },
            success: function(result) {
                var result_list = JSON.parse(result);
                $(".select_price").html(result_list.select_price);

                $(".price_detail button").css("background-color", "#eee");
                $(".price_detail button").css("color", "#212529");
                $(".price_detail button").removeClass("active_select");
                $(".price_detail button[value=" + price_get + "]").css("background-color", "#6c757d");
                $(".price_detail button[value=" + price_get + "]").css("border-color", "#6c757d");
                $(".price_detail button[value=" + price_get + "]").css("color", "#fff");
                $(".price_detail button[value=" + price_get + "]").addClass("active_select");
                $(".result_detail #count").html(result_list.count);
                $(".result_detail #sum_result").html(result_list.sum_result);
                $(".result_detail #sum_shell_result").html(result_list.sum_shell_result);
                $("input:hidden[name=price]").val(price_get);
                $("input:hidden[name=count]").val(result_list.count);
                $("input:hidden[name=sum_result]").val(result_list.sum_result);
                $("input:hidden[name=sum_shell_result]").val(result_list.sum_shell_result);
                console.log(result_list);
            }
        });
    }

    function select_count(count) {
        var count = count.value;
        price = $(".active_select").val();
        $.ajax({
            url: "selectCount_game",
            data: {
                price: price,
                count: count,
                id_card: <?php echo $cardDetail['id']; ?>,
            },
            success: function(result) {
                var result_list = JSON.parse(result);
                $(".result_detail #count").html(result_list.count);
                $(".result_detail #sum_result").html(result_list.sum_result);
                $(".result_detail #sum_shell_result").html(result_list.sum_shell_result);
                $("input:hidden[name=count]").val(result_list.count);
                $("input:hidden[name=sum_result]").val(result_list.sum_result);
                $("input:hidden[name=sum_shell_result]").val(result_list.sum_shell_result);
                console.log(result_list);
            }
        });

    } 

    function successOrder() {
        var priceOrder = $("input:hidden[name=price]").val();
        var countOrder = $("input:hidden[name=count]").val();
        var sum_resultOrder = $("input:hidden[name=sum_result]").val();

        $.ajax({
            url: "order_card",
            method:"POST",
            data: {
                price: priceOrder,
                count: countOrder,
                total: sum_resultOrder,
                id_card: <?php echo $cardDetail['id']; ?>,
            },
            success: function(result) {
                var result_list = JSON.parse(result);
               if (result_list.successfully === false) {
                    $("#message_order").html(result_list.message);
                    $(".order_data_name").css("display","none");
                    $(".text_24beym").css("display","none");
                    $(".text_24beym_arrow").css("display","none");
                    $("#token").val('');
                    $(".table_order").css("display","none");
                    $("#token").css("display","none");
                    $("#copy_token").css("display","none");
                    $("#order").modal("show");
               }else if(result_list.successfully === true){
                    $("#message_order").html(result_list.message);
                    $(".order_data_name").css("display","block");
                    $(".text_24beym").css("display","block");
                    $(".text_24beym_arrow").css("display","block");
                    $("#order_data").html(result_list.order);
                    $("#card_name").html(result_list.card_name);
                    $("#card_price").html(result_list.card_price+" บาท");
                    $("#card_count").html(result_list.card_count+" ใบ");
                    $("#card_total").html(result_list.card_total+" บาท");
                    $("#token").val(result_list.token);
                    $(".table_order").css("display","table");
                    $("#token").css("display","block");
                    $("#copy_token").css("display","block");
                    $("#order").modal("show");
               }
                console.log(result_list);
            }
        });
    }

    function copyToken() {
       var copyText = document.getElementById("token");
           copyText.select();
           copyText.setSelectionRange(0, 99999);
           document.execCommand("copy");

        var tooltip = document.getElementById("copy_token");
            tooltip.innerHTML = "Copy เรียบร้อยแล้ว";
    }
</script>