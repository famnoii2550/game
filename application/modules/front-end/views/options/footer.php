<!-- Footer top section -->
<section class="footer-top-section">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <div class="footer-logo text-white" style="padding-right: 70px;">
                    <img src="public/frontend/assets/img/large-1568709940461.png" alt="">
                    <p>Copyright © 2020 <span><a href="https://www.facebook.com/24buym/?epa=SEARCH_BOX" style="color:#ffb320;">24BUYM</a></span> All rights reserved.</p>
                    <p>เกี่ยวกับเรา<br>เราคือตัวแทนจำหน่าย บัตรเติมเงินเกมต่างๆ มากมาย
                        และ รับเติมเกมมือถือออนไลน์ทุกชนิด</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer-widget mb-5 mb-md-0">
                    <h4 class="fw-title" style="color: #ffb320;">ติดต่อเรา</h4>
                    <div class="latest-blog">
                        <div class="lb-item">
                            <div class="lb-content">
                                <div class="lb-date"><i class="fa fa-phone-square"></i> +66878895360</div>
                            </div>
                            <div class="lb-content">
                                <div class="lb-date"><i class="fa fa-facebook-official"></i> <a href="https://www.facebook.com/24buym/?epa=SEARCH_BOX">24buym</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <div class="footer-widget">
                    <h4 class="fw-title" style="color: #ffb320;">Facebook Fanpage</h4>
                    <div class="top-comment">
                        <div class="tc-item">
                            <div class="tc-content">
                                <div class="fb-page" data-href="https://www.facebook.com/24buym/" data-tabs="event" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                    <blockquote cite="https://www.facebook.com/24buym/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/24buym/">24buym.COM บริการเกมออนไลน์และอุปกรณ์เกมมิ่ง</a></blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer top section end -->


<!-- Footer section -->
<footer class="footer-section">
    <div class="container">
        <ul class="footer-menu">
            <li><a href="index" style="color:<?php if ($this->uri->segment(1) == "index") {
                                                    echo '#ffb320';
                                                } ?>">หน้าหลัก</a></li>
            <li><a href="pomotion" style="color:<?php if ($this->uri->segment(1) == "pomotion") {
                                                    echo '#ffb320';
                                                } ?>">โปรโมชั่น</a></li>
            <li><a href="card_game" style="color:<?php if ($this->uri->segment(1) == "card_game") {
                                                        echo '#ffb320';
                                                    } ?>">บัตรเติมเกมส์</a></li>
            <li><a href="top_up_game" style="color:<?php if ($this->uri->segment(1) == "top_up_game") {
                                                        echo '#ffb320';
                                                    } ?>">เติมเกมส์</a></li>
            <!-- <li><a href="#">ช่วยเหลือ</a></li> -->
        </ul>
        <p class="copyright">
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>
                document.write(new Date().getFullYear());
            </script> จำหน่ายบัตรเติมเกมออนไลน์ ราคาถูก

            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </p>
    </div>
</footer>
<!-- Footer section end -->


<!--====== Javascripts & Jquery ======-->
<script src="public/frontend/assets/js/ajax.js"></script>
<script src="public/frontend/assets/js/popper.js"></script>
<script src="public/frontend/assets/js/jquery-3.2.1.min.js"></script>
<script src="public/frontend/assets/js/bootstrap.min.js"></script>
<script src="public/frontend/assets/js/owl.carousel.min.js"></script>
<script src="public/frontend/assets/js/jquery.marquee.min.js"></script>
<script src="public/frontend/assets/js/main.js"></script>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v6.0&appId=1048048345558237&autoLogAppEvents=1"></script>
<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            xfbml: true,
            version: 'v6.0'
        });
    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<!-- Your send button code -->
<div class="fb-send" data-href="https://www.facebook.com/24buym/" data-layout="button_count">
</div>
</body>

</html>