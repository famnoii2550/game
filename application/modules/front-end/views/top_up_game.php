<style>
	.modal-content12 {
		position: relative;
		display: -ms-flexbox;
		display: flex;
		-ms-flex-direction: column;
		flex-direction: column;
		width: 100%;
		pointer-events: auto;
		/* background-color: #fff; */
		background-clip: padding-box;
		/* border: 1px solid rgba(0, 0, 0, .2); */
		border-radius: .3rem;
		outline: 0;
	}
</style>
<?php $get_promo = $this->db->get('tbl_promotion')->result_array(); ?>
<!-- Latest news section -->
<div class="latest-news-section">
	<div class="ln-title">โปรโมชั่นสำหรับคุณ</div>
	<div class="news-ticker">
		<div class="news-ticker-contant">
			<?php foreach ($get_promo as $key => $get_promo) { ?>
				<div class="nt-item"><span class="new">โปรโมชั่น</span><a href="pomotion" style="color:#fff;"><?php echo $get_promo['title']; ?>. </a></div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Latest news section end -->


<!-- Page info section -->
<section class="page-info-section set-bg" data-setbg="public/frontend/assets/img/review-bg-2.jpg">
	<div class="pi-content">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 col-lg-12 text-center">
					<h2 style="color:orange">เติมเกมส์</h2>
					<p>
						<a href="index" style="color:#ffb320;">หน้าหลัก</a><span style="color:#fff;"> > เติมเกมส์</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Page info section -->


<!-- Page section -->
<section class="page-section review-page spad" style="background-color:rgb(19, 19, 19);">
	<div class="container">
		<div class="row">
			<p style="color:#fff;font-size:24px;">เกมส์ที่คุณสามารถเติมได้</p>
		</div>
		<div class="row">
			<?php
			$i = 1;
			$e = 1;
			?>
			<?php foreach ($get_game as $key => $data) { ?>
				<div class="col-md-6">
					<div class="review-item" data-toggle="modal" data-target="#exampleModalScrollable<?php echo $i++; ?>">
						<div class="review-cover set-bg" style="height:400px;" data-setbg="uploads/game/<?php echo $data['file_name']; ?>">
							<!-- <div class="score yellow">1</div> -->
						</div>
						<div class="review-text">
							<h4><?php echo $data['title']; ?></h4>
						</div>
					</div>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="exampleModalScrollable<?php echo $e++; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
					<div class="modal-dialog modal-dialog-scrollable" role="document">
						<div class="modal-content12">
							<div class="modal-header" style="border-bottom:0">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true" style="color:#fff;font-size:30px;">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<?php if (empty($data['url'])) { ?>
									<img src="uploads/game/<?php echo $data['file_cover']; ?>" alt="">
								<?php } else { ?>
									<a href="<?php echo $data['url']; ?>" target="_blank">
										<img src="uploads/game/<?php echo $data['file_cover']; ?>" alt="">
									</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<!-- Page section end -->