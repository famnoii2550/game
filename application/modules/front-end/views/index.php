<!-- Hero section -->
<section class="hero-section">
	<div class="hero-slider owl-carousel">
		<?php $get_slider = $this->db->order_by('create_at', 'DESC')->get('tbl_slider')->result_array(); ?>
		<?php foreach ($get_slider as $key => $get_slider) { ?>
			<div class="hs-item set-bg" data-setbg="uploads/slider/<?php echo $get_slider['file_name']; ?>"></div>
		<?php } ?>
	</div>
</section>
<!-- Hero section end -->

<?php $get_promo = $this->db->get('tbl_promotion')->result_array(); ?>
<!-- Latest news section -->
<div class="latest-news-section">
	<div class="ln-title">โปรโมชั่นสำหรับคุณ</div>
	<div class="news-ticker">
		<div class="news-ticker-contant">
			<?php foreach ($get_promo as $key => $get_promo) { ?>
				<div class="nt-item"><span class="new">โปรโมชั่น</span><a href="pomotion" style="color:#fff;"><?php echo $get_promo['title']; ?>. </a></div>
			<?php } ?>
		</div>
	</div>
</div>
<!-- Latest news section end -->


<!-- Feature section -->
<!-- <section class="feature-section spad">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 p-0">
				<div class="feature-item set-bg" data-setbg="public/frontend/assets/img/features/1.jpg">
					<span class="cata new">new</span>
					<div class="fi-content text-white">
						<h5><a href="#">Suspendisse ut justo tem por, rutrum</a></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<a href="#" class="fi-comment">3 Comments</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 p-0">
				<div class="feature-item set-bg" data-setbg="public/frontend/assets/img/features/2.jpg">
					<span class="cata strategy">strategy</span>
					<div class="fi-content text-white">
						<h5><a href="#">Justo tempor, rutrum erat id, molestie</a></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<a href="#" class="fi-comment">3 Comments</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 p-0">
				<div class="feature-item set-bg" data-setbg="public/frontend/assets/img/features/3.jpg">
					<span class="cata new">new</span>
					<div class="fi-content text-white">
						<h5><a href="#">Nullam lacinia ex eleifend orci porttitor</a></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<a href="#" class="fi-comment">3 Comments</a>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 p-0">
				<div class="feature-item set-bg" data-setbg="public/frontend/assets/img/features/4.jpg">
					<span class="cata racing">racing</span>
					<div class="fi-content text-white">
						<h5><a href="#">Lacinia ex eleifend orci suscipit</a></h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
						<a href="#" class="fi-comment">3 Comments</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!-- Feature section end -->

<!-- Review section -->
<section class="review-section spad set-bg" data-setbg="public/frontend/assets/img/review-bg.png" style="background-color: #131313;">
	<div class="container">
		<!-- <div class="section-title">
			<div class="cata new">new</div>
			<h2 style="color:#fff;">Recent Reviews</h2>
		</div> -->
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="review-item">
					<div class="review-cover set-bg" style="border-radius:15px;" data-setbg="public/frontend/assets/img/card-1.png">
						<!-- <div class="score yellow">9.3</div> -->
					</div>

				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="review-item">
					<div class="review-cover set-bg" style="border-radius:15px;" data-setbg="public/frontend/assets/img/card-2.png">
						<!-- <div class="score purple">9.5</div> -->
					</div>

				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="review-item">
					<div class="review-cover set-bg" style="border-radius:15px;" data-setbg="public/frontend/assets/img/card-4.png">
						<!-- <div class="score green">9.1</div> -->
					</div>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="logo-payment">
				<img src="public/frontend/assets/img/ture.png" alt="">
				<img src="public/frontend/assets/img/turemoney.png" alt="">
				<img src="public/frontend/assets/img/kbank.png" alt="">
				<img src="public/frontend/assets/img/krungsri.png" alt="">
				<img src="public/frontend/assets/img/krungthai.png" alt="">
				<img src="public/frontend/assets/img/bbl.png" alt="">
				<img src="public/frontend/assets/img/scb.png" alt="">
				<img src="public/frontend/assets/img/paypal.png" alt="">
				<img src="public/frontend/assets/img/prompt-pay.jpg" alt="">
			</div>
		</div>
	</div>
</section>
<!-- Review section end -->