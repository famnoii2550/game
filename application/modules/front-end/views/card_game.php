	<?php $get_promo = $this->db->get('tbl_promotion')->result_array(); ?>
	<!-- Latest news section -->
	<div class="latest-news-section">
		<div class="ln-title">โปรโมชั่นสำหรับคุณ</div>
		<div class="news-ticker">
			<div class="news-ticker-contant">
				<?php foreach ($get_promo as $key => $get_promo) { ?>
					<div class="nt-item"><span class="new">โปรโมชั่น</span><a href="pomotion" style="color:#fff;"><?php echo $get_promo['title']; ?>. </a></div>
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- Latest news section end -->
	<!-- Page info section -->
	<section class="page-info-section set-bg" data-setbg="public/frontend/assets/img/review-bg-2.jpg">
		<div class="pi-content">
			<div class="container">
				<div class="row">
					<div class="col-xl-12 col-lg-12 text-center">
						<h2 style="color:orange">บัตรเติมเกมส์</h2>
						<p>
							<a href="index" style="color:#ffb320;">หน้าหลัก</a><span style="color:#fff;"> > บัตรเติมเกมส์</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Page info section -->

	<!-- Review section -->
	<section class="review-section review-dark spad set-bg" style="background-color:rgb(19, 19, 19);">
		<div class="container">
			<div class="row text-white">
				<?php $get_card = $this->db->get('tbl_card')->result_array(); ?>
				<?php foreach ($get_card as $key => $data) { ?>
					<div class="col-lg-3 col-md-6">
						<a href="card_game_detail?id=<?php echo base64_encode($data['id']); ?>">
							<div class="review-item">
								<div class="review-cover set-bg" data-setbg="uploads/card_game/<?php echo $data['file_name']; ?>">
									<!-- <div class="score yellow">1</div> -->
								</div>
								<div class="review-text">
									<h5><?php echo $data['title']; ?></h5>
									<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisc ing ipsum dolor sit ame.</p> -->
								</div>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</section>
	<!-- Review section end -->